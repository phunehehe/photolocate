#!/usr/bin/env bash
set -efuxo pipefail

bundle install
rake test
rubocop
ruby -I lib bin/photolocate
gem build photolocate.gemspec
