require 'simplecov'
SimpleCov.start

require 'minitest/autorun'
require 'nokogiri'

require 'photolocate'

photo_dir = "#{__dir__}/photos"
gps_photo = "#{photo_dir}/gps.jpg"
minimal_photo = "#{photo_dir}/minimal.jpg"

describe Photo do

  describe 'find_photos' do

    it 'finds a photo' do
      paths = Photo.find_photos(photo_dir).map(&:path)
      paths.must_include minimal_photo
    end

    it 'skips files that are not photos' do
      paths = Photo.find_photos(photo_dir).map(&:path)
      paths.wont_include "#{photo_dir}/empty"
    end
  end

  describe 'latitude' do

    it 'extracts latitude' do
      photo = Photo.new gps_photo
      photo.latitude.must_equal 1
    end

    it 'handles missing data' do
      photo = Photo.new minimal_photo
      photo.latitude.must_be_nil
    end
  end

  describe 'longitude' do

    it 'extracts longitude' do
      photo = Photo.new gps_photo
      photo.longitude.must_equal 2
    end

    it 'handles missing data' do
      photo = Photo.new minimal_photo
      photo.longitude.must_be_nil
    end
  end
end

describe PhotoLocate do

  def with_captured_stdout
    old_stdout = $stdout
    $stdout = StringIO.new
    yield
    $stdout.string
  ensure
    $stdout = old_stdout
  end

  describe 'main' do

    it 'processes the current directory writing to stdout' do
      output = with_captured_stdout do
        Dir.chdir photo_dir do
          PhotoLocate.main
        end
      end
      csv = CSV.parse output
      csv.size.must_equal 2
      csv.first.size.must_be :>, 1
    end

    it 'takes an option for the input directory' do
      output = with_captured_stdout do
        PhotoLocate.main ['--input', photo_dir]
      end
      output.lines.length.must_equal 2
    end

    it 'takes an option for the output file' do
      Dir.mktmpdir do |temp_dir|
        output_file = "#{temp_dir}/test.csv"
        Dir.chdir photo_dir do
          PhotoLocate.main ['--output', output_file]
        end
        File.read(output_file).lines.length.must_equal 2
      end
    end

    it 'takes an option to render html' do
      output = with_captured_stdout do
        # Cannot chdir here because we need to match minimal_photo below
        PhotoLocate.main [
          '--format',
          'html',
          '--input',
          photo_dir,
        ]
      end
      Nokogiri::HTML output, &:strict
      output.must_match minimal_photo
    end

    it 'raises an error when given unknown arguments' do
      proc do
        PhotoLocate.main ['unknown']
      end.must_raise OptionParser::InvalidArgument
    end
  end
end
