let pkgs = import <nixpkgs> {};
in pkgs.stdenv.mkDerivation {
  name = "photolocate";
  buildInputs = with pkgs; [
    ruby
    zlib
  ];
}
