Gem::Specification.new do |s|
  s.name = 'photolocate'
  s.version = '0.0.0'
  s.license = 'MPL-2.0'
  s.summary = 'Find location information in photos'
  s.author = 'Hoang Xuan Phu'

  s.homepage = 'https://gitlab.com/phunehehe/photolocate'
  s.executables = ['photolocate']
  s.files = ['lib/photolocate.rb']
end
