require 'csv'
require 'exifr/jpeg'
require 'find'
require 'optparse'

class Photo

  def self.find_photos(path)
    Find.find(path).map do |p|
      begin
        Photo.new p
      rescue EXIFR::MalformedJPEG, Errno::EISDIR
        nil
      end
    end.compact
  end

  def self.render(format, photos)
    send "render_#{format}", photos
  end

  def self.render_csv(photos)
    photos.map(&:render_csv).join
  end

  def self.render_html(photos)
    "<!doctype html>
    <html>
      <title>photolocate</title>
      <meta charset='utf-8'>
      <body>
        #{photos.map(&:render_html).join}
      </body>
    </html>"
  end

  def initialize(path)
    @path = path
    gps = EXIFR::JPEG.new(path).gps
    @latitude = gps.latitude unless gps.nil?
    @longitude = gps.longitude unless gps.nil?
  end

  attr_reader :path, :latitude, :longitude

  def render_csv
    [
      path,
      latitude,
      longitude,
    ].to_csv
  end

  def render_html
    "<dl>
      <dt>Path</dt>
      <dd>#{path}</dd>
      <dt>Latitude</dt>
      <dd>#{latitude}</dd>
      <dt>Longitude</dt>
      <dd>#{longitude}</dd>
    </dl>"
  end
end

class PhotoLocate

  def self.parse_options(args)
    options = {
      format: 'csv',
      input: '.',
    }
    OptionParser.new do |opts|
      opts.on '--input INPUT (defaults to current directory)' do |i|
        options[:input] = i
      end
      opts.on '--output OUTPUT (defaults to standard output)' do |o|
        options[:output] = o
      end
      opts.on('--format FORMAT (csv or html, defaults to csv)',
              %i[csv html]) do |f|
        options[:format] = f
      end
    end.parse! args
    raise OptionParser::InvalidArgument unless args.empty?
    options
  end

  def self.with_output(output)
    output_fd = if output.nil?
                  $stdout
                else File.open output, 'w'
                end
    yield output_fd
  ensure
    output_fd.close unless output_fd == $stdout
  end

  def self.main(args = [])
    options = parse_options args
    photos = Photo.find_photos options[:input]
    with_output options[:output] do |fd|
      fd.write Photo.render options[:format], photos
    end
  end
end
