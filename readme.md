# photolocate

[![build status](https://gitlab.com/phunehehe/photolocate/badges/master/build.svg)](https://gitlab.com/phunehehe/photolocate/pipelines)
[![coverage report](https://gitlab.com/phunehehe/photolocate/badges/master/coverage.svg)](https://phunehehe.gitlab.io/photolocate/)

`photolocate` looks for location information in photos and outputs CSV or HTML.

Installation:

```
gem build photolocate && gem install photolocate
WARNING:  no email specified
WARNING:  See http://guides.rubygems.org/specification-reference/ for help
  Successfully built RubyGem
  Name: photolocate
  Version: 0.0.0
  File: photolocate-0.0.0.gem
Successfully installed photolocate-0.0.0
1 gem installed
```

Alternatively, just use `ruby -I lib bin/photolocate`.

Quick help:

```
photolocate --help
Usage: photolocate [options]
        --input INPUT (defaults to current directory)
        --output OUTPUT (defaults to standard output)
        --format FORMAT (csv or html, defaults to csv)
```

CSV output:

```
$ photolocate --input test/photos
test/photos/gps.jpg,1.0,2.0
test/photos/minimal.jpg,,
```

HTML output:

```
$ photolocate --input test/photos --format html | tidy -indent -quiet
<!DOCTYPE html>
<html>
<head>
  <meta name="generator" content=
  "HTML Tidy for HTML5 for Linux version 5.2.0">
  <title>photolocate</title>
  <meta charset='utf-8'>
</head>
<body>
  <dl>
    <dt>Path</dt>
    <dd>test/photos/gps.jpg</dd>
    <dt>Latitude</dt>
    <dd>1.0</dd>
    <dt>Longitude</dt>
    <dd>2.0</dd>
  </dl>
  <dl>
    <dt>Path</dt>
    <dd>test/photos/minimal.jpg</dd>
    <dt>Latitude</dt>
    <dd></dd>
    <dt>Longitude</dt>
    <dd></dd>
  </dl>
</body>
</html>
```
